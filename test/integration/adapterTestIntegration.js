/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-webex_teams',
      type: 'WebexTeams',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const WebexTeams = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] WebexTeams Adapter Test', () => {
  describe('WebexTeams Class Tests', () => {
    const a = new WebexTeams(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getPeople - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeople('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'getPeople', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPerson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addPerson('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.nickName);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.avatar);
                assert.equal('string', data.response.orgId);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.licenses));
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.timeZone);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'addPerson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerson('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.nickName);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.avatar);
                assert.equal('string', data.response.orgId);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.licenses));
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.timeZone);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'getPerson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePerson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerson('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.nickName);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.avatar);
                assert.equal('string', data.response.orgId);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.licenses));
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.timeZone);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'updatePerson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePerson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePerson('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'deletePerson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMe((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.nickName);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.avatar);
                assert.equal('string', data.response.orgId);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.licenses));
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.timeZone);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('People', 'getMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRooms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRooms('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Rooms', 'getRooms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRoom - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoom('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.isLocked);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Rooms', 'createRoom', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoom - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoom('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.isLocked);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Rooms', 'getRoom', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRoom - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoom('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.isLocked);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.lastActivity);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Rooms', 'updateRoom', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoom - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoom('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Rooms', 'deleteRoom', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberships - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMemberships('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Memberships', 'getMemberships', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMembership('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.roomId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(true, data.response.isModerator);
                assert.equal(true, data.response.isMonitor);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Memberships', 'createMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMembership('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.roomId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(false, data.response.isModerator);
                assert.equal(true, data.response.isMonitor);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Memberships', 'getMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMembership('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.roomId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(true, data.response.isModerator);
                assert.equal(false, data.response.isMonitor);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Memberships', 'updateMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMembership - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMembership('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Memberships', 'deleteMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMessages('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Messages', 'getMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMessage('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.roomId);
                assert.equal('string', data.response.roomType);
                assert.equal('string', data.response.toPersonId);
                assert.equal('string', data.response.toPersonEmail);
                assert.equal('string', data.response.text);
                assert.equal('string', data.response.markdown);
                assert.equal('string', data.response.html);
                assert.equal(true, Array.isArray(data.response.files));
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.mentionedPeople));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Messages', 'createMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMessage('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.roomId);
                assert.equal('string', data.response.roomType);
                assert.equal('string', data.response.toPersonId);
                assert.equal('string', data.response.toPersonEmail);
                assert.equal('string', data.response.text);
                assert.equal('string', data.response.markdown);
                assert.equal('string', data.response.html);
                assert.equal(true, Array.isArray(data.response.files));
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.mentionedPeople));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Messages', 'getMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMessage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMessage('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Messages', 'deleteMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeams('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Teams', 'getTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTeam('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Teams', 'createTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeam('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Teams', 'getTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTeam('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Teams', 'updateTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeam('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Teams', 'deleteTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamMemberships - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamMemberships('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('TeamMemberships', 'getTeamMemberships', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTeamMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTeamMembership('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(true, data.response.isModerator);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('TeamMemberships', 'createTeamMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamMembership('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(false, data.response.isModerator);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('TeamMemberships', 'getTeamMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTeamMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTeamMembership('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.teamId);
                assert.equal('string', data.response.personId);
                assert.equal('string', data.response.personEmail);
                assert.equal('string', data.response.personDisplayName);
                assert.equal(true, data.response.isModerator);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('TeamMemberships', 'updateTeamMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamMembership - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamMembership('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('TeamMemberships', 'deleteTeamMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebhooks('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Webhooks', 'getWebhooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWebhook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWebhook('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.targetUrl);
                assert.equal('string', data.response.resource);
                assert.equal('string', data.response.event);
                assert.equal('string', data.response.orgId);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.appId);
                assert.equal('string', data.response.ownedBy);
                assert.equal('string', data.response.filter);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Webhooks', 'createWebhook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebhook('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.targetUrl);
                assert.equal('string', data.response.resource);
                assert.equal('string', data.response.event);
                assert.equal('string', data.response.orgId);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.appId);
                assert.equal('string', data.response.ownedBy);
                assert.equal('string', data.response.filter);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Webhooks', 'getWebhook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWebhook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWebhook('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.targetUrl);
                assert.equal('string', data.response.resource);
                assert.equal('string', data.response.event);
                assert.equal('string', data.response.orgId);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.appId);
                assert.equal('string', data.response.ownedBy);
                assert.equal('string', data.response.filter);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Webhooks', 'updateWebhook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebhook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWebhook('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Webhooks', 'deleteWebhook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Organizations', 'getOrganizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganization('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Organizations', 'getOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicenses('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Licenses', 'getLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicense('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.totalUnits);
                assert.equal('string', data.response.consumedUnits);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Licenses', 'getLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoles('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Roles', 'getRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRole('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Roles', 'getRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContent('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-webex_teams-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Contents', 'getContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
