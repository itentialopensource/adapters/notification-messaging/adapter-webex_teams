
## 0.8.4 [10-15-2024]

* Changes made at 2024.10.14_21:13PM

See merge request itentialopensource/adapters/adapter-webex_teams!16

---

## 0.8.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-webex_teams!14

---

## 0.8.2 [08-14-2024]

* Changes made at 2024.08.14_19:26PM

See merge request itentialopensource/adapters/adapter-webex_teams!13

---

## 0.8.1 [08-07-2024]

* Changes made at 2024.08.06_21:09PM

See merge request itentialopensource/adapters/adapter-webex_teams!12

---

## 0.8.0 [07-18-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!11

---

## 0.7.5 [03-29-2024]

* Changes made at 2024.03.29_10:21AM

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!10

---

## 0.7.4 [03-13-2024]

* Changes made at 2024.03.13_11:13AM

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!9

---

## 0.7.3 [03-11-2024]

* Changes made at 2024.03.11_11:13AM

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!8

---

## 0.7.2 [02-28-2024]

* Changes made at 2024.02.28_12:32PM

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!7

---

## 0.7.1 [01-01-2024]

* Update metadata

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!6

---

## 0.7.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!5

---

## 0.6.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!4

---

## 0.5.6 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!3

---

## 0.5.5 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-webex_teams!1

---

## 0.5.4 [01-30-2020] & 0.5.3 [01-30-2020]

- Fixing entitypaths to have slash between version and path

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!6

---

## 0.5.2 [01-02-2020]

- December Migration

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!5

---

## 0.5.1 [11-18-2019]

-Fix healthcheck url and better entitypath format

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!4

---

## 0.5.0 [11-07-2019]

- Migrate to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!3

---

## 0.4.0 [09-12-2019]

- September migration

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-ciscospark!1

---

## 0.1.1 [06-15-2019]

- Initial Commit

See commit b69db4e

---
