# Webex Teams 

Vendor: Cisco Systems 
Homepage: https://www.cisco.com/

Product: Webex Teams 
Product Page: https://www.webex.com/

## Introduction
We classify Webex Teams into the Notifications domain as Webex Teams provides a collaborative platform for messaging, video conferencing, and notifications. 

## Why Integrate
The Webex Teams adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Webex Teams. With this adapter you have the ability to perform operations such as:

- Message

## Additional Product Documentation
[API Documents for Webex](https://developer.webex.com/docs/)